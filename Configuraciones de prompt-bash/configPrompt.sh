#!/bin/bash

# Comentar la línea de la variable PS1 predefinida del usuario.
sed -i 's/^PS1/#&/' ~/.bashrc

# Obtener el número de la línea anteriormente comentada.
NUM_LINE_PS1=$(sed -n '/^#PS1/=' ~/.bashrc)
echo "${NUM_LINE_PS1}"

# Variable que almacena la configuración personal de la variable PS1 del PROMPT.
PS1_IN='PS1="${GRAY}\h${BLUE}linux${CYAN}: ${LIGHT_GREEN}\w${LIGHT_CYAN}\$(__git_ps1) ${GRAY}\d ${LIGHT_CYAN}$(echo -e "\Uf303") C${PINK}o${YELLOW}l${LIGHT_RED}o${LIGHT_GREEN}m${PINK}b${YELLOW}i${LIGHT_GREEN}a ${WHITE}Humana\n${PINK}$(echo -e "\u2B9A") ${LIGHT_CYAN}d${PINK}a${YELLOW}k${LIGHT_RED}a${LIGHT_GREEN}t${PINK}a${YELLOW}c${LIGHT_GREEN}a${CYAN} \$${RESET} "'

# Convierto \ a \\ en la variable PS1_IN para que sea escapado en el comando sed de envío.
PS1_COLOR=$(echo ${PS1_IN} | sed -e 's,\\,\\\\,g')


# Envío la variable PS1_COLOR al archivo ~/.bashrc
#sed -i ${NUM_LINE_PS1}a\ "${PS1_COLOR}" ~/.bashrc
#sed -i ${NUM_LINE_PS1}a\ "source ~/.git-prompt.sh" ~/.bashrc

# Envío la variable PS1_COLOR al archivo ~/.bashrc
sed -i \
	-e ${NUM_LINE_PS1}a\ "source ~/.git-prompt.sh" \
	-e ${NUM_LINE_PS1}a\ "${PS1_COLOR}" \
	~/.bashrc

# Sustituyo los nombres de las variables de colores, por el código de valor correspondiente de cada una, en la variable PS1 del fichero de configuración del PROMPT del usuario ~/.bashrc.
sed -i \
	-e 's/${GRAY}/\\[\\033[1;30m\\]/g' \
	-e 's/${LIGHT_GRAY}/\\[\\033[0;37m\\]/g' \
	-e 's/${CYAN}/\\[\\033[0;36m\\]/g' \
	-e 's/${LIGHT_CYAN}/\\[\\033[1;36m\\]/g' \
	-e 's/${BLUE}/\\[\\033[34m\\]/g' \
	-e 's/${LIGHT_BLUE}/\\[\\e[1;34m\\]/g' \
	-e 's/${GREEN}/\\[\\033[0;32m\\]/g' \
	-e 's/${LIGHT_GREEN}/\\[\\033[1;32m\\]/g' \
	-e 's/${YELLOW}/\\[\\033[1;33m\\]/g' \
	-e 's/${PINK}/\\[\\033[1;35m\\]/g' \
	-e 's/${RED}/\\[\\033[31m\\]/g' \
	-e 's/${LIGHT_RED}/\\[\\033[1;31m\\]/g' \
	-e 's/${PURPLE}/\\[\\033[0;35m\\]/g' \
	-e 's/${WHITE}/\\[\\033[1;37m\\]/g' \
	-e 's/${BROWN}/\\[\033[0;33m\\]/g' \
	-e 's/${BLACK}/\\[\\033[0;30m\\]/g' \
	-e 's/${NONE_COLOR}/\\[\\033[0m\\]/g' \
	-e 's/${RESET}/\\[$(tput sgr0)\\]/g' \
	~/.bashrc	
