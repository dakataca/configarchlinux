#!/bin/bash
## Script base para mis sctipts personales.

# Inico de configuración del texto.
INIT='\e['

# Fin de configuración del texto.
END='\e[0m'

## Colores de las letras del texto.
# Para usar colores en letra Bold, use solo los números desde el 90 al 97.
# Si usa en letra Bold números del 30 al 37 obtendrá los colores del 90 al 97.
BLACK='30'
GRAY='90' # Se puede usar en bold
RED='31'
LIGHT_RED='91' # Se puede usar en bold
ORANGE='32'
LIGHT_GREEN='92' # Se puede usar en bold
ORANGE='33'
YELLOW='93' # Se puede usar en bold
BLUE='34'
LIGHT_BLUE='94' # Se puede usar en bold
MAGENTA='35' 
LIGHT_MAGENTA='95' # Se puede usar en bold
CYAN='36'
LIGHT_CYAN='96' # Se puede usar en bold
LIGHT_GRAY='37'
WHITE='97' # Se puede usar en bold

## Colores del fondo del texto.
B_BLACK='40'
B_GRAY='100'
B_RED='41'
B_LIGHT_RED='101'
B_ORANGE='42'
B_LIGHT_GREEN='102'
B_ORANGE='43'
B_YELLOW='103'
B_BLUE='44'
B_LIGHT_BLUE='104'
B_MAGENTA='45' 
B_LIGHT_MAGENTA='105'
B_CYAN='46'
B_LIGHT_CYAN='106'
B_LIGHT_GRAY='47'
B_WHITE='107'

## Restablece el texto a su derecha al por defecto.
NORMAL=$(tput sgr0)

## Estilos del texto.
BOLD='1'
DARK='2'
ITALIC='3'
UNDERLINED='4'
BLINKING='5' # Parpadeante.
BLINKING_1='6' # Parpadeante.
INVERTED='7' # Color de letra intercambia color con el fondo o viceversa.
HIDDEN='8' # Oculto.
CROSSED='9' # Tachado.
# ERROR \uf00d, \u00D7
# DONE \uf046, \u2611, \u2718


## Verifica la ejecución del comando que se le pase como parámetro .
check_fail(){
	# Si el parámetro no es igual a true
	if [[ $1 -ne true ]]; then
		>&2 echo -e "${INIT}${BOLD};${BLINKING};${LIGHT_RED}mError!${END}\n${INIT}${BOLD};${LIGHT_RED}m\u2718${END} ${INIT}${LIGHT_RED};${DARK}m$1${END}"
	else
		>&2 echo -e "${INIT}${BOLD};${LIGHT_GREEN}m\uf046 Done..!${END}"
	fi
}

announce() {
	>&2 echo -e "${INIT}${BOLD};${YELLOW}m$1...\u023F3${END}"
	
}

sed '' fichero
check_fail $?

echo 'Comando bueno'
check_fail $?

announce ":: Instalando las aplicaciones"
