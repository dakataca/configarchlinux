#!/bin/bash

# Establecer atajos de teclado estandares para copiar (Ctrl + C), cortar (Ctrl + X) y pegar (Ctrl + V) en Vim.
cat <<EOF > ~/.vimrc
vmap <C-c> "+yi
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
imap <C-v> <C-r><C-o>+
EOF

