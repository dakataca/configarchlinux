#!/bin/bash
## Configuración del touchpad de la portatil ASUSK555LB con Archlinux.

# Colores a usar en las salidas
STOP="\e[0m"
GRAY="\e[1;30m"
LIGHT_GRAY="\e[0;37m"
CYAN="\e[0;36m"
LIGHT_CYAN="\e[1;36m"
BLUE="\e[34m"
LIGHT_BLUE="\e[1;34m"
GREEN="\e[0;32m"
LIGHT_GREEN="\e[1;32m"
YELLOW="\e[1;33m"
PINK="\e[1;35m"
RED="\e[31m"
LIGHT_RED="\e[1;31m"
PURPLE="\e[0;35m"
WHITE="\e[1;37m"
BROWN="\e[0;33m"
BLACK="\e[0;30m"
NONE_COLOR="\e[0m"
RESET="$(tput sgr0)"

# Verifica la ejecución del comando que se le pase como parámetro .
check_fail(){
	# Si el parámetro no es igual a true
	if [[ $1 -ne true ]]; then
		>&2 echo -e "${RED}$1${STOP}${RESET}"
	else
		>&2 echo -e "${LIGHT_GREEN}Done!${STOP}${RESET}"
	fi
}

announce() {
FRAMEWORK=${CYAN}
TITLE=${LIGHT_BLUE}
	>&2 echo -e "${FRAMEWORK}........................................................................${STOP}${RESET}"
	>&2 echo -e "${FRAMEWORK}:  ${STOP}${TITLE}$1...${STOP}${RESET}"
	>&2 echo -e "${FRAMEWORK}.......................................................................:${STOP}${RESET}"
	
}


install_dependences(){
	sudo pacman -Sy libinput xf86-input-libinput xorg-xinit  --needed --noconfirm
	check_fail $?
}

# Obtener identificador del touchpad con xinput.
list_properties_touchpad(){
	
	# Obtenemos el identificador numérico del dispositivo touchpad.
	ID_TOUCHPAD=$(xinput list | grep -i 'touchpad' | awk '{ for(i=1;i<=NF;i++) if ($i ~ /id=/) print $i }' | cut -d '=' -f2)
	
	# Listamos las características habilitadas a configurar en nuestro Touchpad antes de configurarlo.
	xinput list-props $ID_TOUCHPAD | sed -n '/Available\|Default/!p'
	
	# xinput list-props $(xinput list | grep -i 'touchpad' | awk '{ for(i=1;i<=NF;i++) if ($i ~ /id=/) print $i }' | cut -d '=' -f2) | sed -n '/Available\|Default/!p'

}

## Configuramos los parámetros caracaterísticos de nuestro Touchpad en específico.
set_properties_touchpad(){

	local SET="sudo xinput set-prop $ID_TOUCHPAD "
	local PROPERTIES=(
	'libinput Tapping Enabled'
	'libinput Tapping Drag Enabled'
       	'libinput Tapping Drag Lock Enabled'
	'libinput Tapping Button Mapping Enabled'
	'libinput Natural Scrolling Enabled'
	'libinput Disable While Typing Enabled'
	'libinput Scroll Method Enabled'
	'libinput Click Method Enabled'
	'libinput Middle Emulation Enabled'
	'libinput Accel Speed'
	'libinput Left Handed Enabled'
	'libinput Send Events Mode Enabled'
	'libinput Drag Lock Buttons'
	'libinput Horizontal Scroll Enabled')
	local VALUES=('1' '1' '1' '{1,0}' '1' '1' '{1,0,0}' '{1,0}' '1' '0.3' '0' '{0,1}' '0' '1')
	for (( i=0; i<${#PROPERTIES[@]}; i++ ))
	do	
		echo "$SET\"${PROPERTIES[${i}]}\" ${VALUES[$i]}"
		echo "$SET\"${PROPERTIES[${i}]}\" ${VALUES[$i]}" | bash			
	done
		
}
announce "Instalando dependencias!"
install_dependences
announce "Listando propiedades del dispositivo!"
list_properties_touchpad
check_fail $?
announce "Configurando parámetros del touchpad!"
set_properties_touchpad
check_fail $?
