#!/bin/bash

## Gestiona las configuraciones personales del usuario para el navegador chromium.
# Enlace de guía: https://wiki.archlinux.org/index.php/Chromium/Tips_and_tricks#Force_a_password_store

# Verifica la ejecución del comando que se le pase como parámetro .
check_fail(){
	# Si el parámetro no es igual a true
	if [[ $1 -ne true ]]; then
		>&2 echo "$1... "
	else
		>&2 echo "Done!"
	fi
}

# Inserta las opciones de configuración.
INSERT_OPTIONS(){
tee <<EOF ~/.config/chromium-flags.conf >/dev/null
#### Configuraciones de chromium para el usuario.

## Iniciar maximizado.
--start-maximized

## Iniciar en modo incógnito.
#--incognito

### Configurar el depósito de llaves
## gnome: usa el llavero de Gnome
## kwallet5: usa la cartera de KDE
## basic: guarda las contraseñas y la clave de cifrado de las cookies como
##   texto sin formato en el archivo Login Data y Deshabilita la solitud de contraseña del depósito de claves.
## detect: el comportamiento predeterminado de detección automática.
--password-store=basic

## Reducir el consumo de memoria RAM.
## https://wiki.archlinux.org/index.php/Profile-sync-daemon#Edit_the_config_file
EOF
}

# Elimina todas las líneas del archivo de configuración.
DELETE_CONTENT(){
	sed -i 'd' ~/.config/chromium-flags.conf
}

# Crea el archivo de configuración vacío.
CREATE_CONFIG_FILE(){
>~/.config/chromium-flags.conf
}

# Si el fichero existe.
if [[ -e ~/.config/chromium-flags.conf ]]; then
	DELETE_CONTENT
	check_fail $?	
	INSERT_OPTIONS	
	check_fail $?
else
	CREATE_CONFIG_FILE
	check_fail $?
	INSERT_OPTIONS
	check_fail $?
fi
