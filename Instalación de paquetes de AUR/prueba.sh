#!/bin/bash

# Establesco el lenguaje a utilizar en wps-office
LANGUAJE='es-MX'

# Establesco la expresión regular para filtrar las líneas de la lista de idiomas disponibles.
regex="[a-z][a-z]-[A-Z][A-Z]"

# Obtener el primer idioma de la lista
INIT=$(sed -n  "/$regex/p"  PKGBUILD | sed -n '1p' | cut -c 10-14)

# Obtener el último idioma de la lista
END=$(sed -n  "/$regex/p"  PKGBUILD | sed -n '$p' | cut -c 10-14)

# Obtener el número de línea del primer idioma de la lista.
NUM_LINE_INIT=$(sed -n /${INIT}/= PKGBUILD)

# Obtener el número de línea del último idioma de la lista.
get_num_line_end_languaje(){
	END=$(sed -n  "/$regex/p"  PKGBUILD | sed -n '$p' | cut -c 10-14)
	NUM_LINE_END=$(sed -n /${END}/= PKGBUILD)
}

# Obtengo el número de línea correspondiente al lenguaje a utilizar.
get_num_line_languaje(){
	NUM_LINE_LANGUAJE=$(sed -n /${LANGUAJE}/= PKGBUILD)
}


# Obtengo el número de línea correspondiente al primer lenguaje en la lista filtrada.
#Innecesaria porque siempre va a ser 1.
NUM_LINE_INIT_LANGUAJE=$(sed -n  "/$regex/p"  PKGBUILD | sed -n /$INIT/=)

# Obtengo el número de línea correspondiente al último lenguaje en la lista filtrada.
NUM_LINE_END_LANGUAJE=$(sed -n  "/$regex/p"  PKGBUILD | sed -n /$END/=)


echo "Número de línea del lenguaje seleccionado: ${NUM_LINE_LANGUAJE}"

echo "Número de línea del primer lenguaje filtrado: ${NUM_LINE_INIT_LANGUAJE}"

echo "Número de línea del último lenguaje filtrado: ${NUM_LINE_END_LANGUAJE}"

get_num_line_end_languaje

delete_languajes(){
	
	# Elimino las líneas de lenguajes antes del seleccionado.
	sed -i $NUM_LINE_INIT,$(($NUM_LINE_LANGUAJE-1))d PKGBUILD

	# Regenero el número de línea del lenguaje seleccionado y el final, porque han sido movidos de línea.
	get_num_line_languaje
	get_num_line_end_languaje
	
	# Elimino las líneas despúes del lenguaje seleccionado.
	sed -i $(($NUM_LINE_INIT+1)),${NUM_LINE_END}d PKGBUILD
}

# Si el número del Lenguaje seleccionado no es ni el primero ni el último.
if [[ $NUM_LINE_LANGUAJE -ne $NUM_LINE_INIT ]] && [[ $NUM_LINE_LANGUAJE -ne $NUM_LINE_END ]]; then
	echo " Número de línea distinto de inicial y final"
	delete_languajes
	
elif [[ $NUM_LINE_LANGUAJE -eq $NUM_LINE_INIT  ]]; then
		echo " Número de línea igual al inicial"
	sed -i $(($NUM_LINE_INIT+1)),${NUM_LINE_END}d PKGBUILD
	
else
		echo " Número de línea igual al final"
	sed -i $(($NUM_LINE_INIT)),$(($NUM_LINE_END-1))d PKGBUILD

fi

#INIT=sed $(sed -n  "/[a-z][a-z]-[A-Z][A-Z]/p"  PKGBUILD | sed -n '1p' | cut -c 10-14)/= PKGBUILD
echo "INIT: $INIT"
echo "NUM_LINE_INIT: $NUM_LINE_INIT"
echo "END: $END"
echo "NUM_LINE_END: $NUM_LINE_END"




#sed "/$(sed -n "\/es-MX\/p" PKGBUILD | cut -c 10-14)/=" PKGBUILD

#sed -n  "/[a-z][a-z]-[A-Z][A-Z]/p"  PKGBUILD; [[ $1 ==  ]];

# Si la primera línea del filtro no corresponde al idioma seleccionado.
#if [[ $(sed -n  "/[a-z][a-z]-[A-Z][A-Z]/p"  PKGBUILD | sed -n '1p' | cut -c 10-14) != "es-MX" ]]; then
	#echo "Primera línea no corresponde al idioma es-MX."
	
	# Si la última línea del filtro no corresponde al idioma seleccionado
	#if [[  ]]; then
		# Elimina las líneas una despúes de la seleccionada hasta el final
		#sed -e $(($NUM_LINE_ES_MX+1)),$(($NUM_LINE_SHASUMS-3))d PKGBUILD
	#fi
	
	# Elimina desde la primera linea hasta una línea antes del número de línea NUM_LINE_ES_MX.
	#sed -e $(($NUM_LINE_LANGUAJES+1)),$(($NUM_LINE_ES_MX-1))d PKGBUILD
#fi

# Elimina desde la primera linea hasta una línea antes del número de línea NUM_LINE_ES_MX.
#sed -e $(($NUM_LINE_LANGUAJES+1)),$(($NUM_LINE_ES_MX-1))d PKGBUILD
#sed -e $(($NUM_LINE_ES_MX+1)),$(($NUM_LINE_SHASUMS-3))d PKGBUILD

# Evaluar coincidencias en el rango de los números de líneas anteriores.
#for var in $(seq $(($NUM_LINE_LANGUAJES+1)) $(($NUM_LINE_SHASUMS-3)))
#do


#sed -n ${var}p PKGBUILD | cut -c 10-14
#sed -n ${var}p PKGBUILD
#echo "var: $var"
#sed -n ${var}d PKGBUILD


# Si el idioma no es igual al latino
#if [[ $(sed -n ${var}p PKGBUILD | cut -c 10-14) != 'es-MX' ]]; then
#	sed -n "/$(sed -n ${var}p PKGBUILD | cut -c 10-14)/=" PKGBUILD
	#echo "var: $var"
	#sed "${var}d" PKGBUILD
#fi


#[[ $(sed -n "${var}p" PKGBUILD | cut -c 10-14) -eq "es-MX" ]]; echo  es igual

#sed -e "${var}d" PKGBUILD



#done