#!/bin/bash

## Este script elimina las llaves PGP y los lenguajes inecesarios del fichero PKGBUILD previo a la instalación
#del paquete de idiomas de la suite ofimática wps-office.

# Establesco el lenguaje a utilizar en wps-office.
LANGUAJE="es-MX"

# Establesco la expresión regular para filtrar la lista de idiomas disponibles.
regex="[a-z][a-z]-[A-Z][A-Z]"

# sed -n  "/$regex/p"  PKGBUILD | sed -n '1,$p' | cut -c 10-15
# Filtra y recorra las líneas correspondientes a los lenguajes disponibles.
for lang in $(sed -n  "/$regex/p"  PKGBUILD | egrep -o "$regex")
do

	# Obtengo el número de línea de cada uno de los idiomas disponibles.
	NUM_LINE_LANG=$(sed -n /$lang/= PKGBUILD)

	#Si el lenguaje no corresponde al seleccionado, borre esa línea.
	if [[ $lang != $LANGUAJE ]]; then
		sed -i ${NUM_LINE_LANG}d PKGBUILD
	fi
done

# Si el lenguaje es Español establesca la llave PGP correspondiente
if [[ $LANGUAJE == "es-MX" ]]; then
	KEY_PGP="'a77e4e44ab9c5db0638a8f86041dbb4e11dde02638c504179a307bc58eaa9db3'"
elif [[ $LANGUAJE == "es-ES" ]]; then
	KEY_PGP="'18fed4c9bae7fc6afcf37de5fbd51f4eb80dd59ab5e417ae81e067f408321c4b'"
else
	echo "Este escript solo admite la configuración al lenguaje español."
fi

# Filtre la lista de las llaves PGP en el fichero PKGBUILD.	
for pgp in $(egrep -o "'.{64}'" PKGBUILD)
do
	# Obtenga el número de línea de la llave PGP leída.
	NUM_LINE_KEY_PGP=$(sed -n /$pgp/= PKGBUILD)	
	
	# Si la llave PGP corresponde al idioma seleccionado entonces.
	if [[ $pgp != $KEY_PGP ]]; then
		
		# Elimine la línea de acuerdo al número de línea obtenido.
		sed -i ${NUM_LINE_KEY_PGP}d PKGBUILD
	fi

done
