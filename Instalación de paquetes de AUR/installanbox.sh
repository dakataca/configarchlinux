#!/bin/bash

sudo pacman -S dkms linux-headers --needed --noconfirm

PACKAGE="properties-cpp anbox-image-gapps anbox-git  anbox-modules-dkms-git anbox-bridge"

for p in $PACKAGE
do
	if pacman -Qm $p &> /dev/null ; then
		echo "$p ya está instalado"
	else
		git clone https://aur.archlinux.org/$p.git
		cd $p
		makepkg -sic --noconfirm
		cd ..
		sudo rm -r $p
	fi
done

sudo modprobe ashmem_linux
sudo modprobe binder_linux

PS3="Desea iniciar el servicio de anbox?:"
select an in "si" "no"
do
	case $REPLY in
		1)echo "Iniciando anbox."
			sudo systemctl start anbox-container-manager.service
			echo "anbox iniciado correctamente!!!"
		break;;
		2)echo "Escogió no iniciar anbox!"
		break;;
		*) echo "Opción incorrecta, elija 1 o 2."
	esac
done

echo "ejecutando anbox!"

anbox-bridge
