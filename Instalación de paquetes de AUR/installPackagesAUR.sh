#!/bin/bash

# Este script instala/actualiza los siguientes paquetes de AUR.

PACKAGE=('
libgksu
gksu
woeusb-git
gradio
paper-icon-theme-git
popcorntime
nodejs-nativefier
whatsapp-nativefier
ttf-ms-fonts
ttf-wps-fonts
wps-office
wps-office-mui-es-mx
wps-office-extension-spanish-dictionary
ttf-monaco
ttf-mac-fonts
nerd-fonts-hack
mcmojave-circle-icon-theme-git
play-with-mpv-git
spek-git
brave-bin
absolutely-proprietary
')

## Verifica la ejecución del comando que se le pase como parámetro .
check_fail(){
	# Si el parámetro no es igual a true
	if [[ $1 -ne true ]]; then
		>&2 echo -e "\e[1;5;91mError!\e[0m\n\e[1;91m\u2718\e[0m \e[91;2m$1\e[0m"
	else
		>&2 echo -e "\e[1;92m\uf046 Done..!\e[0m"
	fi
}


# Imprime un mansaje.
announce() {
	>&2 echo -e "\e[1;93m$1...\u023F3$\e[0m"
	
}

# Verifica si la versión remota es mayor a la local.
VERIFY_VERSION(){
	announce "$1 previamente instalado en el sistema. Verificando versión."
	
	wget -qO PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=$1
	#pacman -Qi ttf-monaco | egrep Versión\|version | sed 's/[0-9]://g ; s/ //g' | cut -d ':' -f2 | cut -d '-' -f1
	#pacman -Qi $1 | egrep Versión\|version | cut -d ':' -f2 | cut -d ' ' -f2  | cut -d '-' -f1
	LOCAL_VERSION=$(pacman -Qi $1 | egrep Versión\|version | sed 's/[0-9]://g ; s/ //g' | cut -d ':' -f2 | cut -d '-' -f1)
	REMOTE_VERSION=$(sed -n '/^pkgver=/p' PKGBUILD | cut -d '=' -f2 | cut -d '-' -f1)
	
	echo $LOCAL_VERSION
	echo $REMOTE_VERSION
	if [[ ${LOCAL_VERSION} == ${REMOTE_VERSION} ]]; then
		echo "Versión igual"
	else
		if [[ ${REMOTE_VERSION} > ${LOCAL_VERSION} ]]; then
			echo "Versión distinta"
			echo "Local versión: ${#LOCAL_VERSION} "
			echo "Remote versión: ${#REMOTE_VERSION} "
			
			if [[ ${#LOCAL_VERSION} -ne ${#REMOTE_VERSION} ]]; then
				echo "Versiones incongruentes."
			else
				INSTALL_PACKAGE $1 "Es necesario actualizar. Actualizando paquete."
				check_fail $?
			fi	

		else
			echo "Última versión instalada"
		
		fi
	fi
	rm PKGBUILD
}

LAT="mx"
ES="es"

# Instala o actualiza un paquete.
INSTALL_PACKAGE(){
	announce "$2"
	local PACKAGE=$1
	local PATH_ACTUAL=$(pwd)
	cd
	
	#if [[ $1 -eq "wps-office-mui-es-${LAT}" ]]; then
	#	$1=$(echo "$1" | sed -e 's/-es-[a-z][a-z]//')
	#fi
	

	# Si es el paquete de idioma español latino mexicano de wps-office
	if [[ $PACKAGE -eq "wps-office-mui-es-${LAT}" ]]; then
		PACKAGE=$(echo "${PACKAGE}" | sed -e 's/-es-[a-z][a-z]//')
	
	# Si el paquete es Spotify registre la llave correspondiente.
	elif [[ $PACKAGE -eq "spotify" ]]; then
		gpg --recv-key 931FF8E79F0876134EDDBDCCA87FF9DF48BF1C90
	fi

	# Si ya existe el directorio elimínelo
	sudo rm -rf $PACKAGE
	
	# Clone el repositorio
	git clone https://aur.archlinux.org/$PACKAGE.git
	cd "$PACKAGE"
	#if [[ $p ==  "spflashtool-bin" ]]; then
		#sed -i /^pkgver/c\pkgver=\"5.1804\" PKGBUILD
		#sed -i /^sha256sums/c\sha256sums=\(\'ff3665842b712367a62bd0d60950020e75ac5ccd4be841e4a33d6a388b1f8f08\' PKGBUILD
		#sudo pacman -Sy android-tools android-udev libpng12 --needed --noconfirm
	#elif


	makepkg -sic --noconfirm
	if [[ $PACKAGE -eq "play-with-mpv-git"  ]]; then
		# enlace simbólico para que el servicio chromium/mpv arranque al inicio
		echo "Creando enlace simbólico de chromium/mpv"
		sleep 3
		mkdir -p ~/.config/autostart/
		ln -sf /usr/share/applications/thann.play-with-mpv.desktop ~/.config/autostart/thann.play-with-mpv.desktop
	fi

	cd ..
	sudo rm -r $PACKAGE
	echo $PATH_ACTUAL
	cd "${PATH_ACTUAL}"
}

for p in $PACKAGE
do
if pacman -Qm $p &> /dev/null ; then
	VERIFY_VERSION $p
	check_fail $?
	
else	
	echo "Falta por instalar $p"
	INSTALL_PACKAGE $p "Instalando paquete."
	check_fail $?	

fi
done

